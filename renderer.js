// This file is required by the index.html file and will
// be executed in the renderer process for that window.
// All of the Node.js APIs are available in this process.
var timer;
var audio = new Audio('pling.mp3')

function buttonClick() {
    var button = document.getElementById('intervalbutton');
    var buttonText = button.innerText;

    if (buttonText === "Start"){
        var myInterval = document.getElementById('intervalinput').value;
        interval = myInterval + '000';
        button.innerText = "Stop";
        timer = setInterval(myTimer, interval);
    } else {
        button.innerText = "Start";
        clearInterval(timer);
    } 
}

function myTimer() {
    audio.play()
}


document.querySelector('#intervalbutton').addEventListener('click', buttonClick)
